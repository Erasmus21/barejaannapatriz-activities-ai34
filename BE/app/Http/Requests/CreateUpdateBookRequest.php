<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateBookRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255'
            ],
            'author' => [
                'required',
                'max:255'
            ],
            'copies' => [
                'required',
                'numeric'
            ],
            'category_id' => [
                'required'
            ]
        ];
    }
}
