<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Borrowed_Book extends Model
{
    use HasFactory;

    public $table = "borrowed_books";

    protected $fillable = ['patron_id', 'copies', 'book_id'];

    public function borrowedBook_book_relation() {
        return $this->belongsTo('App\Book', 'book_id', 'id');
    }

    public function borrowedBook_patron_relation() {
        return $this->belongsTo('App\Patron', 'patron_id', 'id');
    }
}
