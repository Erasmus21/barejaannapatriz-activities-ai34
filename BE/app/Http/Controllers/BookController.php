<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BookResource;
use App\Models\Book;
use App\Http\Requests\CreateUpdateBookRequest;
use App\Http\Requests\CreateBookRequest;


class BookController extends Controller
{
    
    public function index()
    {
        $books = Book::all();
        $data = BookResource::collection($books);
        return response()->json($data);
    }

    public function store(CreateBookRequest $request)
    {      
        $validated = $request->validated();
        
        $input = $request->all();

        $book = Book::create($input);

        if($book->save())
        {
            return response()->json(['message' => 'Book successfuly saved!', $book]);
        }

        return response()->json(['message' => 'Failed to add book', $book]);
    }

    public function show($id)
    {
        $book = Book::findOrFail($id);
        $data = new BookResource($book);
        return response()->json($data);
    }

    public function update(CreateUpdateBookRequest $request, $id)
    {
        $validated = $request->validated();
        
        $updatedBookInfo = [
            'name' => $request->name,
            'author' => $request->author,
            'copies' => $request->copies,
            'category_id' => $request->category_id
        ];

        $updatedBook = Book::where('id', $id)
            ->update($updatedBookInfo);

        return response()->json(['message' => 'Book successfuly updated!', $updatedBook]);
    
    }

    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        
        if($book->delete())
        {
            return response()->json(['message' => 'Book successfuly deleted!', $data]);
        }

        return response()->json(['message' => 'Failed to remove book.', $data]);

    }
}
