import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { API_BASE_URL } from '../assets/js/config.js'

Vue.use(Vuex)

const bookStore = new Vuex.Store(
    {
      state: {
          books: [],
          categories: [],
          borrowed: [],
      },
      getters: {
        getBooks: state => {
          return state.books;
        },
        getCategories: state => {
          return state.categories;
        }
      },
      mutations: {
        setBooks: (state, bookArray) => {
          state.books = bookArray;
        },
        setCategories: (state, categoryArray) => {
          state.categories = categoryArray;
        },
        setNewBook: (state, book) => {
          state.books.unshift({ ...book });
        },
        setNewBorrow: (state, borrowed) => {
          state.books.unshift({ ...borrowed });
        },
        setDeleteBook: (state, id) =>{
          state.books = state.books.filter((book) => book.id !== id)
        }
      },
      actions: {
        fetchBooks({ commit }) {
          axios.get(API_BASE_URL + "/books")
            .then((response) => {
                commit('setBooks', eval(response.data));
              }
            )
            .catch((error) => {
              console.log(error.response.data);
            });
        },
        fetchCategories({ commit }) {
          axios.get(API_BASE_URL + "/categories")
            .then((response) => {
                commit('setCategories', eval(response.data));
              }
            )
            .catch((error) => {
              console.log(error.response.data);
            });
        },
        createBook({commit}, bookArray) {
          axios.post(API_BASE_URL + "/book", bookArray)
            .then((response) => {
              commit("setNewBook", response.data.book);
            })
            .catch((error) => {
              console.log(error.response.data);
            });
        },
        updateBook({dispatch} ,array, id) {
           axios.put(API_BASE_URL + `/books/${id}`, array)
            .then(
              dispatch('fetchBooks')
            )
            .catch((error) => {
              console.error(error.response.data);
            });
        },
        deleteBook({ commit }, id) {
          axios.delete(API_BASE_URL + `/books/${id}`)
            .then(
              commit("setDeleteBook", id)
            )
            .catch(error => {
              console.log(error.response.data);
          })
        },
        borrowBook( { commit }, borrowArray) {
          axios.post(API_BASE_URL + "/borrowedBooks", borrowArray)
            .then((response) => {
              commit("setNewBorrow", response.data.borrowedBooks);
            })
            .catch((error) => {
              console.log(error.response.data);
            });
        },
      }
    }
  )
  
  export {bookStore}

