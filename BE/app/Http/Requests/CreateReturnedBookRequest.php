<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateReturnedBookRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'book_id' => [
                'required',
                'book_id' => 'exists:App\Models\Book, id'
            ],
            'copies' => [
                'required',
                'numeric'
            ],
            'patron_id' => [
                'required',
                'patron_id' => 'exists:App\Models\Patron, id'
            ]
        ];
    }
}
