<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BorrowedBookResource;
use App\Models\Borrowed_Book;
use App\Models\Book;
use App\Http\Requests\CreateBorrowedBookRequest;

class BorrowedBookController extends Controller
{
   
    public function index()
    {
        $borrowedBooks = Borrowed_Book::all();
        $data = BorrowedBookResource::collection($borrowedBooks);
        return response()->json($data);
    }

    public function store(CreateBorrowedBookRequest $request)
    {   
        $validated = $request->validated();

        $borrowedBookInfo = $request->all();    

        $bookId = $request->book_id;
        $copiesToBorrow = $request->copies;

        $getAllBorrowedCopiesOfBook = collect(Borrowed_Book::select('copies')
                ->where('book_id','=', $bookId)->get()->toArray());
        $totalBorrowedCopies = $getAllBorrowedCopiesOfBook->sum('copies');
        $totalBookCopies = Book::select('copies')
                ->where('id','=', $bookId)
                ->first()
                ->copies;

        $sumOfBorrowedCopies =  $copiesToBorrow + $totalBorrowedCopies;

        if($totalBookCopies >= $sumOfBorrowedCopies)
        {   
            $borrowedBook = Borrowed_Book::create($borrowedBookInfo);

            $data = new BorrowedBookResource($borrowedBook);
            return response()->json('Borrow transaction successfully saved!');
        }
        
        return response()->json('There are no more available copies for this book');
    }

    public function show($id)
    {
        $borrowedBook = Borrowed_Book::findOrFail($id);
        $data = new BorrowedBookResource($borrowedBook);
        return response()->json($data);
    }

}
