<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\CategoryResource;
use App\Models\Category;

class CategoryController extends Controller
{

    public function index()
    {
        $category = Category::all();
        $data = CategoryResource::collection($category);
        return response()->json($data);
    }
    
    public function show($id)
    {
        $category = Category::findOrFail($id);
        $data = new CategoryResource($category);
        return response()->json($data);
    }
}

   