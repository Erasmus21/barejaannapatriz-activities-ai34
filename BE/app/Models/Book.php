<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function book_borrowebook_relation() {
        return $this->belongsToMany('App\Patron', 'borrowed_books', 'book_id', 'patron_id');
    } 

    public function book_returnedBook_relation() {
        return $this->belongsToMany('App\Patron', 'returned_books', 'book_id', 'patron_id');
    } 

    public function book_category_relation() {
        return $this->hasMany('App\Category');
    } 
}
