<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Returned_Book extends Model
{
    use HasFactory;

    public $table = "returned_books";

    protected $fillable = ['book_id', 'copies', 'patron_id'];

    public function returnedBook_book_relation() {
        return $this->belongsTo('App\Book', 'book_id', 'id');
    }

    public function returnedBook_patron_relation() {
        return $this->belongsTo('App\Patron', 'patron_id', 'id');
    }
}

