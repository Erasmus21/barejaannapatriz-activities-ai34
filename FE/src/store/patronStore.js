import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { API_BASE_URL } from '../assets/js/config.js'

Vue.use(Vuex)

const patronStore = new Vuex.Store(
    {
      state: {
          patrons: [],
      },
      getters: {
        getPatrons: state => {
          return state.patrons;
        }
      },
      mutations: {
        setPatrons: (state, patronArray) => {
           state.patrons = patronArray
        },
        setNewPatron: (state, patron) => {
          state.patrons.unshift({ ...patron })
        },
        setDeletePatron: (state, id) =>{
          state.patrons = state.patrons.filter((patron) => patron.id !== id)
        },
        setUpdatePatron: (state, updatedPatron, id) => {
          const index = state.patrons.findIndex((patron) => patron.id === id);
          if (index !== -1) {
            state.books.splice(index, 1, { ...updatedPatron });
          }
        },
      },
      actions: {
        fetchPatrons({ commit }) {
          axios.get(API_BASE_URL + "/patrons")
            .then((response) => {
                commit('setPatrons', eval(response.data))
              }
            )
            .catch((error) => {
              console.log(error);
          });
        },
        createPatron({commit}, patronArray) {
          axios.post(API_BASE_URL + "/patron", patronArray)
            .then((response) => {
              commit("setNewPatron", response.data.patron);
            })
            .catch((error) => {
              console.log(error)
            });
        },
        deletePatron({ commit }, id) {
          axios.delete(API_BASE_URL + `/patrons/${id}`)
            .then(
              commit("setDeletePatron", id)
            )
            .catch(err => {
              console.error(err.response.data);
          })
        },
        updatePatron(updateArray, id) {
          axios.post(API_BASE_URL + `/patrons/${id}`, updateArray)
            .catch((err) => {
              console.error(err.response.data);
            });
        },
      }
    }
  )
  
  export {patronStore}

