<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Romance', 'Action', 'Mystery', 'Sci-Fi', 'Horror', 'Classic', 'Fiction', 'Drama'];

        for($i = 0; $i < count($categories); $i++){
            DB::table('categories')->insert([
                'category' => $categories[$i]
            ]);
        }

        \App\Models\Book::factory(10)->create();
       
        \App\Models\Patron::factory(10)->create();
    }
}
