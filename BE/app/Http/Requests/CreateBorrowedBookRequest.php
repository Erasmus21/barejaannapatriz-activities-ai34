<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Book;

class CreateBorrowedBookRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'patron_id' => [
                'required'
            ],
            'copies' => [
                'required',
                'numeric'
            ],
            'book_id' => [
                'required'
            ]
        ];
    }
}
