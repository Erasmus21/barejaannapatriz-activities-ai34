<?php


use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\ReturnedBookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Book Routes------------------------------------
Route::resource('books', BookController::class, [
    'only' => ['index','show', 'destroy', 'update']
]);

Route::resource('book', BookController::class, [
    'only' => ['store']
]);

//Patron Routes-----------------------------------

Route::resource('patrons', PatronController::class, [
    'only' => ['index','show', 'destroy']
]);

Route::resource('patron', PatronController::class, [
    'only' => ['store', 'update']
]);

//Category Routes----------------------------------

Route::resource('categories', CategoryController::class, [
    'only' => ['index','show']
]);

//Borrowed_Book Routes-----------------------------

Route::resource('borrowedBooks', BorrowedBookController::class, [
    'only' => ['index','store', 'show']
]);

//Returned_Book Routes----------------------------

Route::resource('returnedBooks', ReturnedBookController::class, [
    'only' => ['index' ,'store', 'show']
]);
