import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import Patron from '../views/Patron.vue'
import Books from '../views/Books.vue'
import Settings from '../views/Settings.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    alias: '/a'
  },
  {
    path: '/Patron',
    name: 'Patron',
    component: Patron,
    alias: '/b'
  },
  {
    path: '/Books',
    name: 'Books',
    component: Books,
    alias: '/c'
  },
  {
    path: '/Settings',
    name: 'Settings',
    component: Settings,
    alias: '/d'
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
