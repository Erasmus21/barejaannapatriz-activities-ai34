<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    use HasFactory;

    protected $fillable = ['last_name', 'first_name', 'middle_name', 'email'];
    
    public function patron_borrowebook_relation() {
        return $this->belongsToMany('App\Book', 'borrowed_books', 'patron_id', 'book_id');
    } 

    public function patron_returnedBook_relation() {
        return $this->belongsToMany('App\Book', 'returned_books', 'patron_id', 'book_id');
    } 
}
