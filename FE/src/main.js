import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import "vue-toastification/dist/index.css";

import VueGoogleCharts from 'vue-google-charts'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Toast from "vue-toastification";

Vue.config.productionTip = false
Vue.use(VueGoogleCharts)
Vue.use(Toast)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
