<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PatronResource;
use App\Models\Patron;
use App\Http\Requests\CreateUpdatePatronRequest;
use App\Http\Requests\CreatePatronRequest;


class PatronController extends Controller
{

    public function index()
    {
        $patrons = Patron::all();
        $data = PatronResource::collection($patrons);
        return response()->json($data);
    }

    public function store(CreatePatronRequest $request)
    {
        $validated = $request->validated();
        
        $input = $request->all();

        $patron = Patron::create($input);

        if($patron->save())
        {
            return response()->json(['message' => 'Patron successfuly saved!', $patron]);
        }

        return response()->json(['message' => 'Failed to add patron', $patron]);
    }

    public function show($id)
    {
        $patron = Patron::findOrFail($id);
        $data = new PatronResource($patron);
        return response()->json($data);
    }

    public function update(CreateUpdatePatronRequest $request, $id)
    {
        $validated = $request->validated();
        
        $updatedPatronInfo = [
            'last_name' => $request->last_name,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'email' => $request->email
        ];

        Patron::update($updatedBookInfo)
            ->where('id', '=', $id);

        if($updatedPatronInfo->update())
        {
            return response()->json(['message' => 'Patron information successfuly updated!', $updatedPatronInfo]);
        }

        return response()->json(['message' => 'Failed to update patron info.', $updatedPatronInfo]);
    }

    public function destroy($id)
    {
        $patron = Patron::findOrFail($id);

        if($patron->delete())
        {   
            return response()->json(['message' => 'Patron successfuly deleted!', $data]);
        }

        return response()->json(['message' => 'Failed to remove patron.', $data]);
    }
}
