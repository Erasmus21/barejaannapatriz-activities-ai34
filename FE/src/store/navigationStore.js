import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const navigationStore = new Vuex.Store(
    {
      state: {
          activeButton: '',
          activeNavbar: ''
      },
      mutations: {
        setButton(state, name) {
          state.activeButton = name;
        },
        setNavbar(state, name) {
          state.activeNavbar = name;
        }
      },
      getters: {
        getButton: state => {
          return state.activeButton
        },
        getTitle: state => {
          return state.activeNavbar
        }
      }
    }
  )
  
  export {navigationStore}