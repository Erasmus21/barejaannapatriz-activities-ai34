<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ReturnedBookResource;
use App\Models\Returned_Book;
use App\Models\Borrowed_Book;
use App\Models\Book;
use App\Http\Requests\CreateReturnedBookRequest;

class ReturnedBookController extends Controller
{

    public function index()
    {
        $returnedBooks = Returned_Book::all();
        $data = ReturnedBookResource::collection($returnedBooks);
        return response()->json($data);
    }

    public function store(CreateReturnedBookRequest $request)
    {
        $validated = $request->validated();

        $returnedBookInfo = $request->all();

        $copiesToReturn = $returnedBookInfo->copies;

        $borrowedBookArray = collect(Borrowed_Book::select('copies')->get()->toArray());
        $returnedBookArray = collect(Returned_Book::select('copies')->get()->toArray());

        $totalNumberOfBorrowedBooks = $borrowedBookArray->sum('copies');
        $getNumberOfReturnedBooks =  $returnedBookArray->sum('copies');

        $totalNumberOfReturnedBooks = $getNumberOfReturnedBooks +  $copiesToReturn;

        $getBorrowedTransactionId = Borrowed_Book::select('id')
            ->where([
                ['book_id','=', $returnedBookInfo->book_id],
                ['copies','=',  $copiesToReturn],
                ['patron_id','=', $returnedBookInfo->patron_id]
            ])
            ->first();

        if($getBorrowedTransactionId != null)
        {   
            if($totalNumberOfReturnedBooks <= $totalNumberOfBorrowedBooks)
            {
                $returnedBook = Returned_Book::create($returnedBookInfo);

                Borrowed_Book::find($getBorrowedTransactionId)->delete();
    
                $data = new BorrowedBookResource($returnedBook);
                return response()->json(['message' => 'Return transaction successfully saved!', $data]); 
            }
        }

        return response()->json('Return transactions exceed borrow transactions.');

    }

    public function show($id)
    {
        $returnedBook = Returned_Book::findOrFail($id);
        $data = new ReturnedBookResource($returnedBook);
        return response()->json($data);
    }
}
